import { TodoService } from './todo.service';
import { Component, OnInit } from '@angular/core';


// add these 2 lines for auth
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // assuming we get this list from API
  menuList;
  todoList;
  menuTitle = 'My Menu';
  count = 0;
  currentTodo;
  public joke: any;

  public user: any;

  constructor(
    public todoSvc: TodoService,
    public afAuth: AngularFireAuth
  ) { }

  loginWithGoogle() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());

  }

  loginWithFacebook() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider());

  }

  loginWithEmail(email: string, password: string) {
    this.afAuth.auth.signInWithEmailAndPassword(email, password)
  }

  logoutAll() {
    this.afAuth.auth.signOut();
  }

  triggerLoginCount(num: number) {
    this.count += num;
  }

  ngOnInit() {
    this.menuList = [
      { name: 'home', url: '/home' },
      { name: 'about', url: '/about' },
      { name: 'google', url: 'www.google.com' },
    ];
    // this.todoList =
    this.todoSvc.getList().subscribe(
      list => this.todoList = list
    );
    this.getJoke();
    // this.user = this.afAuth.authState;
    this.afAuth.authState.subscribe(
      user => {
        this.user = user;
        console.log(this.user);
      }
    )
  }

  addItem(str: string) {
    this.todoSvc.addItem({
      desc: str
    })
      .then(() => {
        this.currentTodo = '';
      })
      .catch((error) => { console.log(error) });
  }

  markComplete(item: any) {
    item.isCompleted = true;
    this.todoSvc.updateItem(item)
      .then(
      () => { }
      )
      .catch(
      err => console.log(err)
      )
  }

  deleteItem(item: any) {
    this.todoSvc.deleteItem(item)
  }

  getJoke() {
    this.todoSvc.getJoke()
      .subscribe(
      (jokeResponse: any) => {
        this.joke = jokeResponse.value;
        console.log(jokeResponse)
      }
      )
  }

  submitForm(frm: any) {
    console.log(frm)
  }

}
