import { Directive, ElementRef, Input, OnInit, HostListener } from '@angular/core';

@Directive({
  selector: '[appHightlight]'
})
export class HightlightDirective implements OnInit {
  // @Input() appHightlight: string;

  @Input() defaultColor: string;
  @Input() highlightColor: string;

  constructor(
    public el: ElementRef
  ) {
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.highlight(this.highlightColor || this.defaultColor || 'blue');
  }
  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
  }
  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }

  ngOnInit() {
    // this.el.nativeElement.style.backgroundColor = this.highlightColor || 'yellow'
  }

  private hightLight(color: string) {
    this.el.nativeElement.style.backgroundColor = color
  }

}
