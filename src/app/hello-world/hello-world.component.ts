import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.css']
})
export class HelloWorldComponent implements OnInit {

  name: string;
  customer: Customer;
  maletoggle: boolean;
  femaletoggle: boolean;

  ngOnInit() {

  }

  constructor() {
    this.name = 'my name';

    this.customer = {
      name: 'chris',
      age: 12
    }
  }

  alert(param: string) {
    window.alert(param)
  };

  male() {
    this.maletoggle = true;
    this.femaletoggle = false;
  }

  female() {
    this.maletoggle = true;
    this.femaletoggle = false;
  }

}


interface Customer {
  age: number;
  name: string;
}
