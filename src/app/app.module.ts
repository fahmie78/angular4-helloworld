import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import {AngularFireModule} from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './app.component';
import { HelloWorldComponent } from './hello-world/hello-world.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { TodoService } from './todo.service';
import { HttpModule } from '@angular/http';
import { NameAgePipe } from './name-age.pipe';
import { HomePageComponent } from './home-page/home-page.component';
import { CustomerPageComponent } from './customer-page/customer-page.component';
import { HightlightDirective } from './hightlight.directive';

const config = {
    apiKey: 'AIzaSyC3vdv-vSFWKYRk2OSm1GlxMV9jZTCt--I',
    authDomain: 'hello-fahmi.firebaseapp.com',
    databaseURL: 'https://hello-fahmi.firebaseio.com',
    projectId: 'hello-fahmi',
    storageBucket: 'hello-fahmi.appspot.com',
    messagingSenderId: '85191894042'
  };

@NgModule({
  declarations: [
    AppComponent,
    HelloWorldComponent,
    MenuListComponent,
    NameAgePipe,
    HomePageComponent,
    CustomerPageComponent,
    HightlightDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    TodoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
