import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css'], encapsulation: ViewEncapsulation.None

})
export class MenuListComponent implements OnInit {
  @Input()
  list: any[];

  @Input()
  title: string;

  @Output()
  loginClicked: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  clickLogin() {
    // file the login event to tell whoever is interested
    this.loginClicked.emit(2);
  }

}
