import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database'
import { Http } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class TodoService {

  constructor(
    private afDb: AngularFireDatabase,
    private http: Http
  ) { }

  getJoke() {
    const url = 'https://api.icndb.com/jokes/random';
    return this.http.get(url)
      .map(x => x.json())
  }

  getList() {
    // return [
    //   { desc: 'read book', isCompleted: false },
    //   { desc: 'play badminton', isCompleted: true },
    //   { desc: 'go sleep' }
    // ]

    return this.afDb.list('/todos');
  }

  addItem(item: TodoItem) {
    return this.afDb.list('/todos').push(item);
  }

  updateItem(item: any) {
    // this.afDb.object('/todos' + item.$key).update({ desc: item.desc, isCompleted: item.isCompleted})
    return this.afDb.list('/todos')
      .update(item.$key, item)
  }

  deleteItem(item: any) {
    return this.afDb.list('/todos')
      .remove(item)
  }

}

interface TodoItem {
  desc: string;
  isCompleted?: boolean;
}

