import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Routes } from '@angular/router';

@Component({
  selector: 'app-customer-page',
  templateUrl: './customer-page.component.html',
  styleUrls: ['./customer-page.component.css']
})
export class CustomerPageComponent implements OnInit {
name: any
nickName: any
age: number

  constructor(
    public routes: ActivatedRoute
  ) { }

  ngOnInit() {
    this.routes.params.subscribe(
      name => {
        this.name = name.name
      }
    )

    this.routes.queryParams
    .subscribe(
      params => {
        this.nickName = params.nickName;
        this.age = params.age;
      }
    )
  }

}
