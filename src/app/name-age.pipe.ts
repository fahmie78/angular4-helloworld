import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameAge'
})
export class NameAgePipe implements PipeTransform {

  // value is the input: name, age
  // return name is age years old.
  transform(value: any, args?: any): any {
    if (!value) {
      return value;
    }

    const result = value.split(args[0]);

    return `${result[0]} is ${result[1]} years old`;
  }

}
